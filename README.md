= Multiplication

== Introduction

Ce projet avait pour but d'utiliser le javascript afin de pouvoir afficher une table de multiplication puis plusieurs.
Tous cela devait être réalisé en une page en utilisant les outils fournis avec node.js et angular.
Pour ce faire j'ai aussi utilisé le framework css Bulma.

== Présentation du code

=== Création du projet

Tout d'abord j'ai créé le projet `multiplication` avec la commande
[source,bash]
----
ng newmultiplication --style=css --routing=false --strict
----
puis j'ai directement créé un nouveau composant avec la commande
[source,bash]
----
ng generate component tableMultiplication
----
---
=== Controller permettant l'affichage et le calcul de la multiplication

pour récupérer le nombre voulu pour la multiplication j'ai tout de suite modifier le fichier `tableMultiplication.component.ts` en important ce qui me permettera de créer un formulaire

[source,javascript]
----
import { FormControl, FormGroup } from '@angular/forms';
----
voici ci-dessous la classe TableMultiplicationComponent
[source,javascript]
----
export class TableMultiplicationComponent implements OnInit {

  multForm!: FormGroup;
  isSubmitted = false;
  badMultNum = false;
  multTab = [1,2,3,4,5,6,7,8,9,10];
  constructor() { }
  @Output() laMult = new EventEmitter<number>();
  @Input() multNum!: number;

  ngOnInit(): void {
    this.multForm = new FormGroup({
      multNum: new FormControl(''),
    });

  }

  get formControls() { return this.multForm.controls; }

  submit() {
    this.isSubmitted = true;
    console.log("multNum :" + this.multForm.value.multNum);
    if (this.multForm.value.multNum == 0) {
      this.badMultNum = true;
      return;
    } else {
      this.laMult.emit(this.multForm.value.multNum);
    }

  }
}
----
l'objet `multform` me permet de manipuler les valeurs contenu dans le formulaire, le booléan `isSubmitted`
me permet de définir si le formulaire a été validé, le booléan `badMult` me permet de dire quand la valeur communiquée est valable ou pas
. La variable `mulTab` nous servira quand nous calculerons et affichons la mulitplication dans le fichier html.
[source,javascript]
----
@Output() laMult = new EventEmitter<number>();
@Input() multNum!: number;
----
ces deux lignes permettent respectivement de pouvoir envoyer par un event `laMult` et de reçevoir `multNum`.
[source,javascript]
----
ngOnInit(): void {
    this.multForm = new FormGroup({
      multNum: new FormControl(''),
    });
  }
----
cette fonction permet d'exécuter dès l'initialisation de la class le contenu de cette fonction même. En l'occurence elle permet de dire que la variable `multNum` prendra la valeur du `FormControl` nommé multNum.

[source,javascript]
----
submit() {
    this.isSubmitted = true;
    console.log("multNum :" + this.multForm.value.multNum);
    if (this.multForm.value.multNum == 0) {
      this.badMultNum = true;
      return;
    } else {
      this.laMult.emit(this.multForm.value.multNum);
    }

  }
----
La fonction s'executera quand le formulaire sera envoyer grâce à `(ngSubmit)` présent dans le fichier html où le formulaire est créer.
La fonction `submit()` permet tout d'abord de dire que le formulaire a été envoyé en en mettant le booléan `isSubmitted` à true puis d'afficher dans la console la valeur de `multNum`. Ensuite
si la valeur de `multNum` est 0 alors le booléan `badMultNum` vaudra false et nous retournons la fonction. Par contre si ces conditions ne sont pas rempli alors `multNum` sera envoyé grace à l' `EventEmitter`.

Nous passons ensuite au fichier html permettant l'affichage du formulaire et de la table ainsi que le calcul de celle-ci.

[source,html]
----
<section class="section">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-half">
                <h3 class="title is-3">multiplication</h3>
                <form [formGroup]="multForm" (ngSubmit)="submit()">
                    <div class="field">
                        <label class="label">Number to show the multiplication table</label>
                        <div class="control">
                            <p [ngClass]="{ 'has-error': isSubmitted && badMultNum }">
                                <input class="input is-success" formControlName="multNum" type="number"
                                    placeholder="multNum" value="">
                            </p>
                            <div *ngIf="isSubmitted && badMultNum" class="help-block">
                                <div *ngIf="badMultNum">Un chiffre est demandé !</div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-link">Soumettre</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
----
Les 5 premières lignes ne servant qu'à mettre le formulaire en forme grâce à `bulma`
je ne les expliquerais pas. La balise `<form>` permet de créer le formulaire en définissant le `formGroup` ansi qu'en spécifiant que la fonction à exécuter à l'envoi du formulaire sera la fonction `submit()`.

[source,html]
----
<p [ngClass]="{ 'has-error': isSubmitted && badMultNum }">
    <input class="input is-success" formControlName="multNum" type="number" placeholder="chiffre à choisir" value="">
</p>
----
`[ngClass]` permet de définir une classe en l'occurence la classe `has-error` dans le contenu de la balise p quand les conditions précisé sont remplie, dans ce cas le formulaire doit être envoyer et le booléan `badMultNum` doit être à `True`.
La balise `input` permet de renseigner la valeur que va avoir `multNum`.

[source,html]
----
<div *ngIf="isSubmitted && badMultNum" class="help-block">
    <div *ngIf="badMultNum">Un chiffre est demandé !</div>
</div>
----

ces lignes permettent grâce à `*ngIf` d'exécuter le contenu de celles-ci si les conditions sont remplies, ici on veut afficher `un chiffre est demandé` si le formulaire à été envoyé mais que le contenu de `multNum` est vide ou qu'il est égale à 0


Pour calculer la table j'ai procédé ainsi :

[source,html]
----
<div class="container">
    <div class="columns is-centered">
        <div class="column is-half">
            <div *ngIf="multNum">
                <h1>Table de {{multNum}}</h1>
                <ul *ngFor="let i of multTab">
                    <li>{{ multNum }}x{{ i }} = {{ multNum * i }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
----
Si `multNum` n'est pas nul alors on affiche et exécute le contenu de la div. `*ngFor` permet d'exécuter le contenu de la liste tant que le tableau `multTab` est parcouru. La variable `i` prend la valeur du tableau au fur et à mesure que le tableau est parcouru permettant de calculer alors le résultat de la multiplication entre `i` et `multNum` grâce à la synthax mustache.


---

=== Les plusieurs tables de multiplication

J'ai créer un composent `tablesMultiplication` pour réaliser cette partie.

Le procédé est à peu près le même c'ets pourquoi je ne vais pas rentrer dans certain détail.

La partie controller de ce composent :
[source,javascript]
----
export class TablesMultiplicationComponent implements OnInit {

  tableMultForm!: FormGroup;
  isSubmitted = false;
  badTableMultNum = false;
  tab = [1,2,3,4,5,6,7,8,9,10];
  tabchiffre = Array();
  @Output() leTableMultNum = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
    this.tableMultForm = new FormGroup({
      tableMultNum: new FormControl(''),
    });
  }

  get formControls() { return this.tableMultForm.controls; }

  submit2(){
    this.tabchiffre=[];
      for(let i = 1; i<=this.tableMultForm.value.tableMultNum; i++){
        this.tabchiffre.push(i)
      }
      console.table(this.tabchiffre);
  }
----
Je vais m'attarder seulement sur les différences entre le premier composant car ils sont asser similaire concernant le formulaire.
Le tableau `tab` permet d'avoir un tableau avec tous les chiffres de 1 à 10.
La fonction `submit2()` permettera de définir toutes variables nécessaire au calcul et à l'affichage des tables dans le fichier html ci-dessous.

[source,html]
----
<section class="section">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-half">
                <form [formGroup]="tableMultForm" (ngSubmit)="submit2()">
                    <div class="field">
                        <label class="label">Choisir un chiffre</label>
                        <div class="control">
                            <p [ngClass]="{ 'has-error': isSubmitted && badTableMultNum }">
                                <select formControlName="tableMultNum">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </p>
                            <div *ngIf="isSubmitted && badTableMultNum" class="help-block">
                                <div *ngIf="badTableMultNum">Un chiffre est demandé !</div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-link">Soumettre</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
----

Comme pour le premier composant le formulaire exécute la fonction `submit2()` etc... cependant cette fois-ci j'ai décidé de passer par un `select`.

[source,html]
----
<div class="container">
    <div class="columns is-centered">
        <div class="column is-half">
            <ul *ngFor="let i of tabchiffre">
                <li>Voici la table de {{ i }}</li>
                <li>---------------</li>
                <li *ngFor="let numbers of tab">| {{ numbers }} X {{ tabchiffre[i-1] }} =
                    {{ numbers * tabchiffre[i-1] }} |
                </li>
                <li>---------------</li>
            </ul>
        </div>
    </div>
</div>
----

Voici le plus grand changement, en effet cette fois-ci je passe par deux `*ngFor`.

---

=== Pour finir le app.component.html

[source,html]
----
<div *ngIf="getMult(); then multiply else nomultiply"></div>
<ng-template #multiply>
  <app-tableMultiplication  (laMult)="onMultAdded($event)" [multNum]="multNum"></app-tableMultiplication>
  <app-tablesMultiplication></app-tablesMultiplication>
</ng-template>
<ng-template #nomultiply>
  <app-tableMultiplication  (laMult)="onMultAdded($event)"></app-tableMultiplication>
</ng-template>
<app-footer></app-footer>
----
la première ligne permet de dire que si la fonction `getMult()` renvoie vrai alors `multiply` sinon `nomultiply` ce qui permet de jouer avec les `ng-template` pour afficher des choses si un chiffre à été choisi ou non.

