import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {
  title = 'TableMultiplication';

  identForm!: FormGroup;
  isSubmitted = false;
  badNombre = false;
  nb = 0;
  

  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      nombre: new FormControl(''),
    });
  }

  get formControls() { return this.identForm.controls; }

  ident() {
    this.isSubmitted = true;
    console.log("nombre :" + this.identForm.value.nombre); 
    if (this.identForm.value.nombre == '') {
      this.badNombre = true;
      return;
    } else{
      this.nb = this.identForm.value.nombre;
      return;
    }
  }
 
  }