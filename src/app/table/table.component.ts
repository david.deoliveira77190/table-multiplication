import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {

  table = false;
  @Input() nombre!: number;
  mul = [
    {num: 1},
    {num: 2},
    {num: 3},
    {num: 4},
    {num: 5},
    {num: 6},
    {num: 7},
    {num: 8},
    {num: 9},
    {num: 10}
  ];

  constructor() { }

  laTable(){
    if (this.nombre >= 1) {
      this.table = true;
      return;
    }
  }
}
