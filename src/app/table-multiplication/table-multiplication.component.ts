import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-table-multiplication',
  templateUrl: './table-multiplication.component.html',
  styleUrls: ['./table-multiplication.component.scss']
})
export class TableMultiplicationComponent implements OnInit {

  nbForm!: FormGroup;
  isSubmitted = false;
  badNombre = false;
  table = false;
  nombre = 0;
  mul = [
    {num: 1},
    {num: 2},
    {num: 3},
    {num: 4},
    {num: 5},
    {num: 6},
    {num: 7},
    {num: 8},
    {num: 9},
    {num: 10}
  ];

  constructor() { }

  ngOnInit(): void {
    this.nbForm = new FormGroup({
      nombre: new FormControl(''),
    });
  }

  get formControls() { return this.nbForm.controls; }

  nb() {
    this.isSubmitted = true;
    console.log("nombre :" + this.nbForm.value.nombre); 
    if (this.nbForm.value.nombre <=1) {
      this.badNombre = true;
      return;
    } else{
      this.table = true;
      this.nombre = this.nbForm.value.nombre;
      return;
    }
  }
}
